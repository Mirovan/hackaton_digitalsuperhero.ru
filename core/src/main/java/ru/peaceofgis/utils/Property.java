package ru.peaceofgis.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Properties;

public class Property {

    private static final Logger LOGGER = LoggerFactory.getLogger(Property.class);

    private final String primaryPropFileName = "/etc/bigint/config.properties";   //Конфиг первичный
    private final String secondaryPropFileName = "application.properties";  //Конфиг локальный (если не найден первичный)

    private Properties properties;

    public Properties loadPropertyValues() throws IOException {
        if (properties == null) loadPropertyValuesFromConfigFile();
        return properties;
    }

    private void loadPropertyValuesFromConfigFile() throws IOException {
        InputStream inputStream = null;
        try {
            String propFileName;

            properties = new Properties();

            File f = new File(primaryPropFileName);
            if(f.exists() && f.isFile()) {
                LOGGER.info("Используется внешний файл конфигурации");
                propFileName = primaryPropFileName;
                FileInputStream fis = new FileInputStream(primaryPropFileName);
                inputStream = new BufferedInputStream(fis);
            } else {
                LOGGER.info("Используется внутренний файл конфигурации");
                propFileName = secondaryPropFileName;
                inputStream = getClass().getClassLoader().getResourceAsStream(secondaryPropFileName);
            }

            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                LoggerUtil.writeError(LOGGER, "property file '" + propFileName + "' not found in the classpath");
            }
        } catch (Exception e) {
            LoggerUtil.writeError(LOGGER, e.getMessage());
        } finally {
            inputStream.close();
        }
    }


    public static String getProperty(String propertyName) {
        Property property = new Property();
        Properties properties = null;
        try {
            properties = property.loadPropertyValues();
        } catch (IOException e) {
            LoggerUtil.writeError(LOGGER, e.getMessage());
        }
        return properties.getProperty(propertyName);
    }


}