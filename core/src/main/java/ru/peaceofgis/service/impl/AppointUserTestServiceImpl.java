package ru.peaceofgis.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.peaceofgis.service.iface.AppointUserTestService;
import ru.peaceofgis.data.dao.AppointUserTestDAO;
import ru.peaceofgis.data.entity.AppointUserTest;
import ru.peaceofgis.data.entity.User;

import java.util.List;

@Service("appointUserTestService")
public class AppointUserTestServiceImpl implements AppointUserTestService {

    @Autowired
    @Qualifier("appointUserTestDAO")
    AppointUserTestDAO appointUserTestDAO;


    @Override
    public AppointUserTest getById(Integer id) {
        return appointUserTestDAO.findById(id).get();
    }


    @Override
    public List<AppointUserTest> getAll() {
        return appointUserTestDAO.findAll();
    }


    @Override
    public List<AppointUserTest> getByUser(User user) {
        return appointUserTestDAO.findByUser(user);
    }


    @Override
    public void add(AppointUserTest appointUserTest) {
        appointUserTestDAO.save(appointUserTest);
    }

}
