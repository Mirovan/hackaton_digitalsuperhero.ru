package ru.peaceofgis.service.impl;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.peaceofgis.service.iface.QuestionService;
import ru.peaceofgis.service.iface.TestCategoryService;
import ru.peaceofgis.service.iface.TestService;
import ru.peaceofgis.data.dao.TestDAO;
import ru.peaceofgis.data.entity.Question;
import ru.peaceofgis.data.entity.Test;
import ru.peaceofgis.data.entity.TestCategory;
import ru.peaceofgis.utils.LoggerUtil;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Service("testService")
public class TestServiceImpl implements TestService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("testDAO")
    private TestDAO testDAO;

    @Autowired
    private TestCategoryService testCategoryService;

    @Autowired
    private QuestionService questionService;

    @Override
    public Test getById(Integer id) {
        return testDAO.findById(id).get();
    }

    @Override
    public List<Test> getAll() {
        return testDAO.findAll();
    }

    @Override
    public Integer add(Test test) {
        test = testDAO.save(test);
        return test.getId();
    }

    @Override
    public Integer add(String json) {
        Integer resultId = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            JsonNode inputNode = objectMapper.readTree(json);
            String name = inputNode.get("name").asText();
            Integer categoryId = inputNode.get("category_id").asInt();
            Integer maxQuestions = inputNode.get("max_questions").asInt();
            String durationSt = inputNode.get("duration").asText();
            Boolean enableCorrectAnswers = inputNode.get("enable_correct_answers").asBoolean();

            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
            Date parsedDate = dateFormat.parse(durationSt);
            Timestamp duration = new Timestamp(parsedDate.getTime());

            TestCategory testCategory = testCategoryService.getById(categoryId);

            List<Question> questions = new ArrayList<>();
            JsonNode questionNode = inputNode.get("questions");
            Iterator<JsonNode> it = questionNode.elements();
            while (it.hasNext()) {
                Integer questionId = Integer.valueOf( it.next().asText() );
                questions.add( questionService.getById(questionId) );
            }

            Test test = new Test(testCategory, name, maxQuestions, questions, duration, enableCorrectAnswers);
            resultId = add(test);

        } catch (IOException e) {
            LoggerUtil.writeError(LOGGER, "Ошибка чтения JSON: " + e.getMessage());
        } catch (ParseException e) {
            LoggerUtil.writeError(LOGGER, "Ошибка чтения JSON: " + e.getMessage());
        }

        return resultId;
    }

}
