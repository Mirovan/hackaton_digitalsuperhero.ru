package ru.peaceofgis.service.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.peaceofgis.data.entity.*;
import ru.peaceofgis.service.iface.AnswerService;
import ru.peaceofgis.service.iface.AttemptQuestionService;
import ru.peaceofgis.service.iface.AttemptSolutionService;
import ru.peaceofgis.service.iface.UserAttemptService;
import ru.peaceofgis.data.dao.UserAttemptDAO;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service("userAttemptService")
public class UserAttemptServiceImpl implements UserAttemptService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("userAttemptDAO")
    private UserAttemptDAO userAttemptDAO;

    @Autowired
    private AnswerService answerService;

    @Autowired
    private AttemptQuestionService attemptQuestionService;

    @Autowired
    private AttemptSolutionService attemptSolutionService;

    @Override
    public UserAttempt getById(Integer id) {
        return userAttemptDAO.findById(id).get();
    }

    @Override
    public UserAttempt getByUID(String uid) {
        return userAttemptDAO.findByUid(uid);
    }

    @Override
    public List<UserAttempt> getAll() {
        return userAttemptDAO.findAll();
    }

    @Override
    public Integer add(UserAttempt userAttempt) {
        return userAttemptDAO.save(userAttempt).getId();
    }

    @Override
    public Integer update(UserAttempt userAttempt) {
        return userAttemptDAO.save(userAttempt).getId();
    }

    @Override
    public List<UserAttempt> getByAppoint(AppointUserTest appointUserTest) {
        return userAttemptDAO.findByAppointUserTest(appointUserTest);
    }

    @Override
    public AttemptResult getResult(UserAttempt userAttempt) {
        //Получаем все вопросы пользователя
        List<AttemptQuestion> attemptQuestions = attemptQuestionService.getByAttempt(userAttempt);

        //число вопросов в тесте = максимальному баллу
        int questionCount = userAttempt.getAppointUserTest().getTest().getMaxQuestions();

        //число правильных ответов
        int correctSolutionCount = 0;
        //число баллов за правильные ответы
        float userAttemptGrade = 0f;

        //Получаем все ответы пользователя
        for (AttemptQuestion attemptQuestion: attemptQuestions) {
            //Все правильные ответы
            List<Answer> correctAnswersList = answerService.getCorrectByQuestion(attemptQuestion.getQuestion());
            Set<Integer> answersSet = new HashSet<>();
            for (Answer answer: correctAnswersList) {
                answersSet.add(answer.getId());
            }

            //Оценка за ответ на текущий вопрос
            float solutionGrade = 0f;

            //Стоимость одного правильного ответа округленное до 2 знаков после запятой
            float correctAnswerWeight = (float) Math.round((float) 1 / answersSet.size() * 100) / 100;

            //Ответы пользователя для текущего вопроса
            List<AttemptSolution> attemptSolutions = attemptSolutionService.getByAttemptQuestion(attemptQuestion);
            //делаем предположение что пользователь НЕправильно ответил на вопрос
            boolean isCorrectSolution = false;
            for (AttemptSolution attemptSolution: attemptSolutions) {
                //Если пользователь дал ответ на вопрос
                if (attemptSolution != null) {
                    Answer userAnswer = attemptSolution.getAnswer();

                    //Сравниваем ответ пользователя с правильными
                    if (answersSet.contains(userAnswer.getId())) {
                        answersSet.remove(userAnswer.getId());  //Удаляем чтобы не повторять проверку
                        solutionGrade += correctAnswerWeight;
                        isCorrectSolution = true;
                    } else {
                        solutionGrade = 0;
                        isCorrectSolution = false;
                        break;
                    }
                } else {
                    isCorrectSolution = false;
                }
            }
            //Если пользователь выбрал не все правильные ответы, то не защитываем как полностью правильный
            if (answersSet.size() > 0) isCorrectSolution = false;

            //Увеличиваем балл
            userAttemptGrade += solutionGrade;
            //Увеличиваем число правильных ответов
            if (isCorrectSolution) correctSolutionCount += 1;
        }

        return new AttemptResult(userAttempt, questionCount, correctSolutionCount, userAttemptGrade);
    }
}
