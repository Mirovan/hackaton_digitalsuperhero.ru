package ru.peaceofgis.service.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.peaceofgis.service.iface.AttemptResultService;
import ru.peaceofgis.data.dao.AttemptResultDAO;
import ru.peaceofgis.data.entity.AttemptResult;
import ru.peaceofgis.data.entity.UserAttempt;

@Service("attemptResult")
public class AttemptResultServiceImpl implements AttemptResultService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("attemptResultDAO")
    private AttemptResultDAO attemptResultDAO;

    @Override
    public AttemptResult getByAttempt(UserAttempt userAttempt) {
        return attemptResultDAO.findByUserAttempt(userAttempt);
    }

    @Override
    public Integer add(AttemptResult attemptResult) {
        return attemptResultDAO.save(attemptResult).getId();
    }
}
