package ru.peaceofgis.service.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.peaceofgis.service.iface.QuestionCategoryService;
import ru.peaceofgis.data.dao.QuestionCategoryDAO;
import ru.peaceofgis.data.entity.QuestionCategory;

@Service("questionCategoryService")
public class QuestionCategoryServiceImpl implements QuestionCategoryService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("questionCategoryDAO")
    private QuestionCategoryDAO questionCategoryDAO;

    @Override
    public QuestionCategory getById(Integer id) {
        return questionCategoryDAO.findById(id).get();
    }

    @Override
    public Integer add(QuestionCategory questionCategory) {
        questionCategory = questionCategoryDAO.save(questionCategory);
        return questionCategory.getId();
    }

}
