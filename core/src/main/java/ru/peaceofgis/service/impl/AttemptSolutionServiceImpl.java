package ru.peaceofgis.service.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.peaceofgis.service.iface.AttemptSolutionService;
import ru.peaceofgis.data.dao.AttemptSolutionDAO;
import ru.peaceofgis.data.entity.AttemptQuestion;
import ru.peaceofgis.data.entity.AttemptSolution;

import java.util.List;

@Service("attemptSolution")
public class AttemptSolutionServiceImpl implements AttemptSolutionService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("attemptSolutionDAO")
    private AttemptSolutionDAO attemptSolutionDAO;

    @Override
    public Integer add(AttemptSolution attemptSolution) {
        return attemptSolutionDAO.save(attemptSolution).getId();
    }

    @Override
    public List<AttemptSolution> getByAttemptQuestion(AttemptQuestion attemptQuestion) {
        return attemptSolutionDAO.findByAttemptQuestion(attemptQuestion);
    }
}