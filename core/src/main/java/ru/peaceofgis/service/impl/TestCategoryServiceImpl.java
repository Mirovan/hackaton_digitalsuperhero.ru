package ru.peaceofgis.service.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.peaceofgis.service.iface.TestCategoryService;
import ru.peaceofgis.data.dao.TestCategoryDAO;
import ru.peaceofgis.data.entity.TestCategory;

@Service("testCategoryService")
public class TestCategoryServiceImpl implements TestCategoryService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("testCategoryDAO")
    private TestCategoryDAO testCategoryDAO;

    @Override
    public Integer add(TestCategory testCategory) {
        testCategory = testCategoryDAO.save(testCategory);
        return testCategory.getId();
    }

    @Override
    public TestCategory getById(Integer id) {
        return testCategoryDAO.findById(id).get();
    }

}
