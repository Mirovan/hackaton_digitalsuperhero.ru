package ru.peaceofgis.service.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.peaceofgis.service.iface.QuestionTypeService;
import ru.peaceofgis.data.dao.QuestionTypeDAO;
import ru.peaceofgis.data.entity.QuestionType;

@Service("questionTypeService")
public class QuestionTypeServiceImpl implements QuestionTypeService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("questionTypeDAO")
    private QuestionTypeDAO questionTypeDAO;

    @Override
    public QuestionType findByCode(String code) {
        return questionTypeDAO.findByCode(code);
    }

}
