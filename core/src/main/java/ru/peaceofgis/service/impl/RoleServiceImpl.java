package ru.peaceofgis.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.peaceofgis.service.iface.RoleService;
import ru.peaceofgis.data.dao.RoleDAO;
import ru.peaceofgis.data.entity.Role;

@Service("roleService")
public class RoleServiceImpl implements RoleService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("roleDAO")
    private RoleDAO roleDAO;

    @Override
    public Role findRoleByCode(String code) {
        return roleDAO.findByCode(code);
    }
}
