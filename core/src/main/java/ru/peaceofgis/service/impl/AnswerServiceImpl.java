package ru.peaceofgis.service.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.peaceofgis.service.iface.AnswerService;
import ru.peaceofgis.data.dao.AnswerDAO;
import ru.peaceofgis.data.entity.Answer;
import ru.peaceofgis.data.entity.Question;

import java.util.List;

@Service("answerService")
public class AnswerServiceImpl implements AnswerService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("answerDAO")
    private AnswerDAO answerDAO;


    @Override
    public Answer getById(Integer id) {
        return answerDAO.findById(id).get();
    }

    @Override
    public List<Answer> getByQuestion(Question question) {
        return answerDAO.findByQuestion(question);
    }



    @Override
    public List<Answer> getCorrectByQuestion(Question question) {
        return answerDAO.findByQuestionAndCorrect(question, true);
    }

    @Override
    public Integer add(Answer answer) {
        answer = answerDAO.save(answer);
        return answer.getId();
    }

}
