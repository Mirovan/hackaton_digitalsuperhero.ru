package ru.peaceofgis.service.impl;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.peaceofgis.data.entity.*;
import ru.peaceofgis.service.iface.AnswerService;
import ru.peaceofgis.service.iface.QuestionCategoryService;
import ru.peaceofgis.service.iface.QuestionService;
import ru.peaceofgis.service.iface.QuestionTypeService;
import ru.peaceofgis.data.dao.QuestionDAO;
import ru.peaceofgis.utils.LoggerUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service("questionService")
public class QuestionServiceImpl implements QuestionService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("questionDAO")
    private QuestionDAO questionDAO;

    @Autowired
    private QuestionCategoryService questionCategoryService;

    @Autowired
    private QuestionTypeService questionTypeService;

    @Autowired
    private AnswerService answerService;

    @Override
    public Question getById(Integer id) {
        return questionDAO.findById(id).get();
    }

    @Override
    public Integer add(Question question) {
        question = questionDAO.save(question);
        return question.getId();
    }

    /**
     * Возвращает все вопросы из теста
     * */
    @Override
    public List<Question> getAllByTest(Test test) {
        List<Question> questions = questionDAO.findAllByTests(test);
        return questions;
    }


    /**
     * Добавляет массив вопросов
     * */
    @Override
    public List<Integer> add(String json) {
        List<Integer> res = new ArrayList<>();

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            JsonNode inputNode = objectMapper.readTree(json);
            JsonNode questionNode = inputNode.get("questions");
            Iterator<JsonNode> it = questionNode.elements();
            while (it.hasNext()) {
                JsonNode item = it.next();
                Integer questionId = add(item);
                res.add(questionId);
            }
        } catch (IOException e) {
            LoggerUtil.writeError(LOGGER, "Ошибка чтения JSON: " + e.getMessage());
        }

        return res;
    }


    /**
     * Добавляет один вопрос
     * */
    private Integer add(JsonNode inputNode) {
        String name = inputNode.get("name").asText();
        String categoryId = inputNode.get("category_id").asText();
        String questionTypeCode = inputNode.get("question_type").asText();
        String text = inputNode.get("text").asText();
        String description = inputNode.get("description").asText();

        QuestionCategory testCategory = questionCategoryService.getById(Integer.valueOf(categoryId));
        QuestionType questionType = questionTypeService.findByCode(questionTypeCode);

        Question question = new Question(testCategory, questionType, name, text, true, description);

        Integer resultId = null;
        if (question != null) {
            resultId = add(question);   //Добавляем Question
            if (resultId != null) {
                JsonNode answersNode = inputNode.get("answers");
                Iterator<JsonNode> it = answersNode.elements();
                while (it.hasNext()) {
                    JsonNode item = it.next();
                    String answerText = item.get("text").asText();
                    Boolean correct = item.get("correct").asBoolean();
                    Answer answer = new Answer(question, answerText, correct);
                    answerService.add(answer);
                }
            }
        }

        return resultId;
    }

}
