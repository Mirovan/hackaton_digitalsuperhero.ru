package ru.peaceofgis.service.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.peaceofgis.service.iface.AttemptQuestionService;
import ru.peaceofgis.service.iface.QuestionService;
import ru.peaceofgis.data.dao.AttemptQuestionDAO;
import ru.peaceofgis.data.entity.AttemptQuestion;
import ru.peaceofgis.data.entity.Question;
import ru.peaceofgis.data.entity.Test;
import ru.peaceofgis.data.entity.UserAttempt;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service("attemptQuestion")
public class AttemptQuestionServiceImpl implements AttemptQuestionService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("attemptQuestionDAO")
    private AttemptQuestionDAO attemptQuestionDAO;

    @Autowired
    private QuestionService questionService;


    @Override
    public AttemptQuestion getById(Integer id) {
        return attemptQuestionDAO.findById(id).get();
    }

    /**
     * Формирует рандомные вопросы из теста
     * */
    @Override
    public List<AttemptQuestion> getRandomByTest(Test test, UserAttempt userAttempt, Integer maxQuestions) {
        //Получаем все вопросы для теста
        List<Question> questions = questionService.getAllByTest(test);

        List<AttemptQuestion> attemptQuestions = new ArrayList<>();

        //Выбираем случайный вопрос и добавляем его в итоговый список
        for (int i=0; questions.size() > 0 && i < maxQuestions; i++) {
            //Выбираем случайный
            Random random = new Random();
            int randomItem = random.nextInt(questions.size());
            //Создаем объект
            AttemptQuestion attemptQuestion = new AttemptQuestion(userAttempt, questions.get(randomItem));
            attemptQuestions.add(attemptQuestion);
            //Удаляем чтобы не добавить его снова
            questions.remove(randomItem);
        }
        return attemptQuestions;
    }

    @Override
    public List<AttemptQuestion> getByAttempt(UserAttempt userAttempt) {
        return attemptQuestionDAO.findByUserAttempt(userAttempt);
    }

    @Override
    public Integer add(AttemptQuestion attemptQuestion) {
        return attemptQuestionDAO.save(attemptQuestion).getId();
    }

    @Override
    public void add(List<AttemptQuestion> attemptQuestions) {
        for (AttemptQuestion aq: attemptQuestions) {
            add(aq);
        }
    }

}
