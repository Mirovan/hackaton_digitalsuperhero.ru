package ru.peaceofgis.service.iface;

import ru.peaceofgis.data.entity.AttemptQuestion;
import ru.peaceofgis.data.entity.Test;
import ru.peaceofgis.data.entity.UserAttempt;

import java.util.List;

public interface AttemptQuestionService {
    AttemptQuestion getById(Integer id);
    List<AttemptQuestion> getRandomByTest(Test test, UserAttempt userAttempt, Integer maxQuestions);
    List<AttemptQuestion> getByAttempt(UserAttempt userAttempt);
    Integer add(AttemptQuestion attemptQuestion);
    void add(List<AttemptQuestion> attemptQuestions);

}
