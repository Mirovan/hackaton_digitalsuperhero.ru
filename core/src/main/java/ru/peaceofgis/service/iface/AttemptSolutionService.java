package ru.peaceofgis.service.iface;

import ru.peaceofgis.data.entity.AttemptQuestion;
import ru.peaceofgis.data.entity.AttemptSolution;

import java.util.List;

public interface AttemptSolutionService {
    Integer add(AttemptSolution attemptSolution);
    List<AttemptSolution> getByAttemptQuestion(AttemptQuestion attemptQuestion);
}
