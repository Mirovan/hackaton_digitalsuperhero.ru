package ru.peaceofgis.service.iface;

import ru.peaceofgis.data.entity.TestCategory;

public interface TestCategoryService {
    Integer add(TestCategory testCategory); //добавление, возвращает ID сущности
    TestCategory getById(Integer id);
}
