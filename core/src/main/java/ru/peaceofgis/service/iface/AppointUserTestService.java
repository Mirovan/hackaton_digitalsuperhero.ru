package ru.peaceofgis.service.iface;

import org.springframework.stereotype.Service;
import ru.peaceofgis.data.entity.AppointUserTest;
import ru.peaceofgis.data.entity.User;

import java.util.List;

@Service("appointUserTestService")
public interface AppointUserTestService {
    AppointUserTest getById(Integer id);
    List<AppointUserTest> getAll();
    List<AppointUserTest> getByUser(User user);
    void add(AppointUserTest appointUserTest);

}
