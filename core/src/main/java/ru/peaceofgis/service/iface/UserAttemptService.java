package ru.peaceofgis.service.iface;

import ru.peaceofgis.data.entity.AppointUserTest;
import ru.peaceofgis.data.entity.AttemptResult;
import ru.peaceofgis.data.entity.UserAttempt;

import java.util.List;

public interface UserAttemptService {
    UserAttempt getById(Integer id);
    UserAttempt getByUID(String uid);
    List<UserAttempt> getAll();
    Integer add(UserAttempt userAttempt);
    Integer update(UserAttempt userAttempt);
    List<UserAttempt> getByAppoint(AppointUserTest appointUserTest);
    AttemptResult getResult(UserAttempt userAttempt);

}
