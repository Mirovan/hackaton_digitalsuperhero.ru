package ru.peaceofgis.service.iface;

import ru.peaceofgis.data.entity.QuestionType;

public interface QuestionTypeService {
    QuestionType findByCode(String code);
}
