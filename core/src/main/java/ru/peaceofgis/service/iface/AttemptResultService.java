package ru.peaceofgis.service.iface;

import ru.peaceofgis.data.entity.AttemptResult;
import ru.peaceofgis.data.entity.UserAttempt;

public interface AttemptResultService {
    AttemptResult getByAttempt(UserAttempt userAttempt);
    Integer add(AttemptResult attemptResult);
}
