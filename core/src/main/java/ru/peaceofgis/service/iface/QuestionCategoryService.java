package ru.peaceofgis.service.iface;

import ru.peaceofgis.data.entity.QuestionCategory;

public interface QuestionCategoryService {
    QuestionCategory getById(Integer id);
    Integer add(QuestionCategory questionCategory); //добавление, возвращает ID сущности
}
