package ru.peaceofgis.service.iface;

import ru.peaceofgis.data.entity.Role;

public interface RoleService {
    Role findRoleByCode(String code);
}
