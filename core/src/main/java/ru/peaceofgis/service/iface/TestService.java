package ru.peaceofgis.service.iface;

import ru.peaceofgis.data.entity.Test;

import java.util.List;

public interface TestService {
    Test getById(Integer id);
    List<Test> getAll();
    Integer add(Test test);
    Integer add(String json); //добавление, возвращает ID сущности

}
