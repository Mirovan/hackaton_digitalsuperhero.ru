package ru.peaceofgis.service.iface;

import ru.peaceofgis.data.entity.Question;
import ru.peaceofgis.data.entity.Test;

import java.util.List;

public interface QuestionService {
    Question getById(Integer id);
    List<Question> getAllByTest(Test test);
    Integer add(Question question); //добавление, возвращает ID сущности
    List<Integer> add(String json); //добавление, возвращает массив ID сущностей
}
