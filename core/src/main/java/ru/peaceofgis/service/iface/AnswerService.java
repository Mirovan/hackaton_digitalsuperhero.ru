package ru.peaceofgis.service.iface;

import ru.peaceofgis.data.entity.Answer;
import ru.peaceofgis.data.entity.Question;

import java.util.List;

public interface AnswerService {
    Answer getById(Integer id);
    List<Answer> getByQuestion(Question question); //получаем все варианты ответов для вопроса
    List<Answer> getCorrectByQuestion(Question question);   //получаем только корректные ответы для вопроса
    Integer add(Answer answer); //добавление, возвращает ID сущности

}
