package ru.peaceofgis.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.peaceofgis.data.entity.AppointUserTest;
import ru.peaceofgis.data.entity.UserAttempt;

import java.util.List;

@Repository("userAttemptDAO")
public interface UserAttemptDAO extends JpaRepository<UserAttempt, Integer> {
    UserAttempt findByUid(String uid);
    List<UserAttempt> findByAppointUserTest(AppointUserTest appointUserTest);
}