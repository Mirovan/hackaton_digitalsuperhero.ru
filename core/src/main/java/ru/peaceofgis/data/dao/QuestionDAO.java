package ru.peaceofgis.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.peaceofgis.data.entity.Question;
import ru.peaceofgis.data.entity.Test;

import java.util.List;

@Repository("questionDAO")
public interface QuestionDAO extends JpaRepository<Question, Integer> {
    List<Question> findAllByTests(Test test);
}