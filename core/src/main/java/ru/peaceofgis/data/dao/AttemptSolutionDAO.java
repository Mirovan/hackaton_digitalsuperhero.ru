package ru.peaceofgis.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.peaceofgis.data.entity.AttemptQuestion;
import ru.peaceofgis.data.entity.AttemptSolution;

import java.util.List;

@Repository("attemptSolutionDAO")
public interface AttemptSolutionDAO extends JpaRepository<AttemptSolution, Integer> {
    List<AttemptSolution> findByAttemptQuestion(AttemptQuestion attemptQuestion);
}