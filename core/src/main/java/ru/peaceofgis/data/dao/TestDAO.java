package ru.peaceofgis.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.peaceofgis.data.entity.Test;

@Repository("testDAO")
public interface TestDAO extends JpaRepository<Test, Integer> {
}