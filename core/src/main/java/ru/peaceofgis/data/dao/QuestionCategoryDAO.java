package ru.peaceofgis.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.peaceofgis.data.entity.QuestionCategory;

@Repository("questionCategoryDAO")
public interface QuestionCategoryDAO extends JpaRepository<QuestionCategory, Integer> {
}