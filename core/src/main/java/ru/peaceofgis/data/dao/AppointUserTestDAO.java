package ru.peaceofgis.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.peaceofgis.data.entity.AppointUserTest;
import ru.peaceofgis.data.entity.User;

import java.util.List;

@Repository("appointUserTestDAO")
public interface AppointUserTestDAO extends JpaRepository<AppointUserTest, Integer> {
    List<AppointUserTest> findByUser(User user);
}