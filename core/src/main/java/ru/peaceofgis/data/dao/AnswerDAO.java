package ru.peaceofgis.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.peaceofgis.data.entity.Answer;
import ru.peaceofgis.data.entity.Question;

import java.util.List;

@Repository("answerDAO")
public interface AnswerDAO extends JpaRepository<Answer, Integer> {
    List<Answer> findByQuestion(Question question);
    List<Answer> findByQuestionAndCorrect(Question question, Boolean correct);
}