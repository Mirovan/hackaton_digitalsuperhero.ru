package ru.peaceofgis.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.peaceofgis.data.entity.TestCategory;

@Repository("testCategoryDAO")
public interface TestCategoryDAO extends JpaRepository<TestCategory, Integer> {
}