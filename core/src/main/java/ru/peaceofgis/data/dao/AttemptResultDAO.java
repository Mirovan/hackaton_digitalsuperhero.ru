package ru.peaceofgis.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.peaceofgis.data.entity.AttemptResult;
import ru.peaceofgis.data.entity.UserAttempt;

@Repository("attemptResultDAO")
public interface AttemptResultDAO extends JpaRepository<AttemptResult, Integer> {
    AttemptResult findByUserAttempt(UserAttempt userAttempt);
}