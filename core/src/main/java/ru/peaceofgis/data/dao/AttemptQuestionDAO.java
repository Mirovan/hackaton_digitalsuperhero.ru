package ru.peaceofgis.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.peaceofgis.data.entity.AttemptQuestion;
import ru.peaceofgis.data.entity.UserAttempt;

import java.util.List;

@Repository("attemptQuestionDAO")
public interface AttemptQuestionDAO extends JpaRepository<AttemptQuestion, Integer> {
    List<AttemptQuestion> findByUserAttempt(UserAttempt userAttempt);
}