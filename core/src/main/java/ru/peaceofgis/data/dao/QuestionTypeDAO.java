package ru.peaceofgis.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.peaceofgis.data.entity.QuestionType;

@Repository("questionTypeDAO")
public interface QuestionTypeDAO extends JpaRepository<QuestionType, Integer> {
    QuestionType findByCode(String code);
}