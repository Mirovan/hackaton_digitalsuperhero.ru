package ru.peaceofgis.data.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.transaction.annotation.Transactional;
import ru.peaceofgis.data.entity.helper.AttemptStatus;

import java.sql.Timestamp;
import java.util.UUID;
import javax.persistence.*;

@Entity
@Table(name = "user_attempt")
@Transactional
@ApiModel(description = "Попытка пользователя")
public class UserAttempt {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(notes = "Уникальный идентификатор", hidden = true)
    private Integer id;

    @Column(name = "uid")
    @ApiModelProperty(notes = "Уникальный идентификатор попытки", hidden = true)
    private String uid;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_appoint")
    @ApiModelProperty(notes = "Назначенный тест")
    private AppointUserTest appointUserTest;

    @Column(name = "startdate")
    @ApiModelProperty(notes = "Начало тестирования")
    private Timestamp startdate;

    @Column(name = "enddate")
    @ApiModelProperty(notes = "Конец тестирования")
    private Timestamp enddate;

    @OneToOne(
            mappedBy = "userAttempt",
            cascade = CascadeType.MERGE,
            fetch = FetchType.EAGER,
            optional = true)
    private AttemptResult attemptResult;

    @Transient
    private AttemptStatus attemptStatus;

    public UserAttempt() {
    }

    public UserAttempt(User user, AppointUserTest appointUserTest, Timestamp startdate, Timestamp enddate) {
        UUID uid = UUID.randomUUID();
        this.uid = uid.toString();
        this.appointUserTest = appointUserTest;
        this.startdate = startdate;
        this.enddate = enddate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public AppointUserTest getAppointUserTest() {
        return appointUserTest;
    }

    public void setAppointUserTest(AppointUserTest appointUserTest) {
        this.appointUserTest = appointUserTest;
    }

    public Timestamp getStartdate() {
        return startdate;
    }

    public void setStartdate(Timestamp startdate) {
        this.startdate = startdate;
    }

    public Timestamp getEnddate() {
        return enddate;
    }

    public void setEnddate(Timestamp enddate) {
        this.enddate = enddate;
    }

    public AttemptResult getAttemptResult() {
        return attemptResult;
    }

    public void setAttemptResult(AttemptResult attemptResult) {
        this.attemptResult = attemptResult;
    }

    public AttemptStatus getAttemptStatus() {
        return attemptStatus;
    }

    public void setAttemptStatus(AttemptStatus attemptStatus) {
        this.attemptStatus = attemptStatus;
    }
}