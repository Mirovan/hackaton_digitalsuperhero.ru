package ru.peaceofgis.data.entity.helper;

public enum AttemptStatus {
    NOT_STARTING,
    IN_PROGRESS,
    FINISHED;

    AttemptStatus() {
    }
}
