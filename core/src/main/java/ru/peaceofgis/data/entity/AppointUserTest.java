package ru.peaceofgis.data.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "appoint_user_test")
@Transactional
@ApiModel(description = "Назначенный пользователю тест")
public class AppointUserTest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(notes = "Уникальный идентификатор", hidden = true)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_test")
    @ApiModelProperty(notes = "Тест")
    private Test test;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_user")
    @ApiModelProperty(notes = "Пользователь")
    private User user;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_user_appointer")
    @ApiModelProperty(notes = "Кто назначил тест")
    private User appointer;

    @Column(name = "appointdate")
    @ApiModelProperty(notes = "Время назначения теста")
    private Timestamp appointDate;

    @Column(name = "startdate")
    @ApiModelProperty(notes = "Начало доступности теста для пользователя")
    private Timestamp startDate;

    @Column(name = "enddate")
    @ApiModelProperty(notes = "Конец доступности теста для пользователя")
    private Timestamp endDate;

    @Column(name = "max_attempt_count")
    @ApiModelProperty(notes = "Максимальное число попыток прохождения теста")
    private Integer maxAttemptCount;

    @OneToMany(mappedBy = "appointUserTest", fetch = FetchType.LAZY)
    private List<UserAttempt> userAttempts;

    public AppointUserTest() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getAppointer() {
        return appointer;
    }

    public void setAppointer(User appointer) {
        this.appointer = appointer;
    }

    public Timestamp getAppointDate() {
        return appointDate;
    }

    public void setAppointDate(Timestamp appointDate) {
        this.appointDate = appointDate;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public Integer getMaxAttemptCount() {
        return maxAttemptCount;
    }

    public void setMaxAttemptCount(Integer maxAttemptCount) {
        this.maxAttemptCount = maxAttemptCount;
    }

    public List<UserAttempt> getUserAttempts() {
        return userAttempts;
    }

    public void setUserAttempts(List<UserAttempt> userAttempts) {
        this.userAttempts = userAttempts;
    }
}
