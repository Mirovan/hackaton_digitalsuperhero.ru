package ru.peaceofgis.data.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.*;

@Entity
@Table(name = "attempt_solution")
@Transactional
@ApiModel(description = "Ответ пользователя на вопрос в попытке")
public class AttemptSolution {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(notes = "Уникальный идентификатор", hidden = true)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_attempt_question")
    @ApiModelProperty(notes = "Вопрос для попытки пользователя")
    private AttemptQuestion attemptQuestion;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_answer")
    @ApiModelProperty(notes = "Ответы")
    private Answer answer;

    public AttemptSolution() {
    }

    public AttemptSolution(AttemptQuestion attemptQuestion, Answer answer) {
        this.attemptQuestion = attemptQuestion;
        this.answer = answer;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AttemptQuestion getAttemptQuestion() {
        return attemptQuestion;
    }

    public void setAttemptQuestion(AttemptQuestion attemptQuestion) {
        this.attemptQuestion = attemptQuestion;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }
}