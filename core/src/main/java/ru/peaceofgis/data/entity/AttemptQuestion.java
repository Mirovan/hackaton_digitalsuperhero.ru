package ru.peaceofgis.data.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.*;

@Entity
@Table(name = "attempt_question")
@Transactional
@ApiModel(description = "Вопросы для пользователя на вопрос в попытке")
public class AttemptQuestion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(notes = "Уникальный идентификатор", hidden = true)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_attempt")
    @ApiModelProperty(notes = "Попытка пользователя")
    private UserAttempt userAttempt;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_question")
    @ApiModelProperty(notes = "Вопрос")
    private Question question;

    public AttemptQuestion() {
    }

    public AttemptQuestion(UserAttempt userAttempt, Question question) {
        this.userAttempt = userAttempt;
        this.question = question;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserAttempt getUserAttempt() {
        return userAttempt;
    }

    public void setUserAttempt(UserAttempt userAttempt) {
        this.userAttempt = userAttempt;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

}