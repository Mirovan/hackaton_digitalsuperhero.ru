package ru.peaceofgis.data.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "test")
@Transactional
@ApiModel(description = "Тест")
public class Test {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ApiModelProperty(notes = "Максимальное число вопросов в тесте")
    @Column(name = "max_question")
    private Integer maxQuestions;

    @ApiModelProperty(notes = "Категория")
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_category")
    private TestCategory testCategory;

    @ApiModelProperty(notes = "Название теста")
    @Column(name = "name")
    private String name;

    @ApiModelProperty(notes = "Вопросы теста")
    @ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinTable(
            name = "test_question",
            joinColumns = @JoinColumn(name = "id_test", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_question", referencedColumnName = "id")
    )
    private List<Question> questions;

    @ApiModelProperty(notes = "Длительность по времени")
    @Column(name = "duration")
    private Timestamp duration;

    @ApiModelProperty(notes = "Доступны ли правильные ответы в конце теста")
    @Column(name = "enable_correct_answers")
    private Boolean enableCorrectAnswers;

    public Test() {
    }

    public Test(TestCategory testCategory, String name, Integer maxQuestions, List<Question> questions, Timestamp duration, Boolean enableCorrectAnswers) {
        this.testCategory = testCategory;
        this.name = name;
        this.maxQuestions = maxQuestions;
        this.questions = questions;
        this.duration = duration;
        this.enableCorrectAnswers = enableCorrectAnswers;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMaxQuestions() {
        return maxQuestions;
    }

    public void setMaxQuestions(Integer maxQuestions) {
        this.maxQuestions = maxQuestions;
    }

    public TestCategory getTestCategory() {
        return testCategory;
    }

    public void setTestCategory(TestCategory testCategory) {
        this.testCategory = testCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public Timestamp getDuration() {
        return duration;
    }

    public void setDuration(Timestamp duration) {
        this.duration = duration;
    }

    public Boolean getEnableCorrectAnswers() {
        return enableCorrectAnswers;
    }

    public void setEnableCorrectAnswers(Boolean enableCorrectAnswers) {
        this.enableCorrectAnswers = enableCorrectAnswers;
    }
}