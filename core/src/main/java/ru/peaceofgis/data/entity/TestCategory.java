package ru.peaceofgis.data.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.*;

@Entity
@Table(name = "test_category")
@Transactional
@ApiModel(description = "Категория теста")
public class TestCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(notes = "Уникальный идентификатор", hidden = true)
    private Integer id;

    @Column(name = "name")
    @ApiModelProperty(notes = "Название категории")
    private String name;

    public TestCategory() {
    }

    public TestCategory(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}