package ru.peaceofgis.data.entity.helper;

import io.swagger.annotations.ApiModel;

import java.util.List;

@ApiModel(description = "Ответ а вопрос")
public class Solution {
    private List<Integer> answerIds;
    private Integer attemptQuestionId;

    public Solution() {
    }

    public List<Integer> getAnswerIds() {
        return answerIds;
    }

    public void setAnswerIds(List<Integer> answerIds) {
        this.answerIds = answerIds;
    }

    public Integer getAttemptQuestionId() {
        return attemptQuestionId;
    }

    public void setAttemptQuestionId(Integer attemptQuestionId) {
        this.attemptQuestionId = attemptQuestionId;
    }
}