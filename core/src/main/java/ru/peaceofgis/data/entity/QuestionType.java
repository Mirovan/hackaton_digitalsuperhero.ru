package ru.peaceofgis.data.entity;

import io.swagger.annotations.ApiModelProperty;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.*;

@Entity
@Table(name = "question_type")
@Transactional
public class QuestionType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(notes = "Уникальный идентификатор", hidden = true)
    private Integer id;

    @Column(name = "code")
    @ApiModelProperty(notes = "Код")
    private String code;

    @Column(name = "name")
    @ApiModelProperty(notes = "Название")
    private String name;

    public QuestionType() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}