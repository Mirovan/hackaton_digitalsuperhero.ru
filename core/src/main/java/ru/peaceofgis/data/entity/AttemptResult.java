package ru.peaceofgis.data.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.*;

@Entity
@Table(name = "attempt_result")
@Transactional
@ApiModel(description = "Результат прохождения теста")
public class AttemptResult {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(notes = "Уникальный идентификатор", hidden = true)
    private Integer id;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_attempt")
    @ApiModelProperty(notes = "Попытка пользователя")
    private UserAttempt userAttempt;

    @Column(name = "question_count")
    @ApiModelProperty(notes = "Вопросов в тесте")
    private Integer questionCount;

    @Column(name = "correct_solution_count")
    @ApiModelProperty(notes = "Число правильных ответов")
    private Integer correctSolutionCount;

    @Column(name = "grade")
    @ApiModelProperty(notes = "Число набранных баллов")
    private Float grade;

    public AttemptResult() {
    }

    public AttemptResult(UserAttempt userAttempt, Integer questionCount, Integer correctSolutionCount, Float grade) {
        this.userAttempt = userAttempt;
        this.questionCount = questionCount;
        this.correctSolutionCount = correctSolutionCount;
        this.grade = grade;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserAttempt getUserAttempt() {
        return userAttempt;
    }

    public void setUserAttempt(UserAttempt userAttempt) {
        this.userAttempt = userAttempt;
    }

    public Integer getQuestionCount() {
        return questionCount;
    }

    public void setQuestionCount(Integer questionCount) {
        this.questionCount = questionCount;
    }

    public Integer getCorrectSolutionCount() {
        return correctSolutionCount;
    }

    public void setCorrectSolutionCount(Integer correctSolutionCount) {
        this.correctSolutionCount = correctSolutionCount;
    }

    public Float getGrade() {
        return grade;
    }

    public void setGrade(Float grade) {
        this.grade = grade;
    }
}
