package ru.peaceofgis.data.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "question")
@Transactional
@ApiModel(description = "Вопрос")
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(notes = "Уникальный идентификатор", hidden = true)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_category")
    @ApiModelProperty(notes = "ID категории")
    private QuestionCategory questionCategory;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_type")
    @ApiModelProperty(notes = "ID типа вопроса")
    private QuestionType questionType;

    @Column(name = "question_name")
    @ApiModelProperty(notes = "Заголовок вопроса")
    private String name;

    @Column(name = "question_text")
    @ApiModelProperty(notes = "Текст вопроса")
    private String text;

    @Column(name = "enable")
    @ApiModelProperty(notes = "Доступность вопроса", hidden = true)
    private Boolean enable;

    @Column(name = "createdate")
    @ApiModelProperty(notes = "Дата создания", hidden = true)
    private Timestamp createdate;

    @OneToMany(mappedBy = "question", fetch = FetchType.LAZY)
    private List<Answer> answers;

    @ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinTable(
            name = "test_question",
            joinColumns = @JoinColumn(name = "id_question", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_test", referencedColumnName = "id")
    )
    private List<Test> tests;

    @Column(name = "description")
    @ApiModelProperty(notes = "Комментарий правильного ответа")
    private String description;


    public Question() {
    }

    public Question(QuestionCategory questionCategory, QuestionType questionType, String name, String text, Boolean enable, String description) {
        this.questionCategory = questionCategory;
        this.questionType = questionType;
        this.name = name;
        this.text = text;
        this.enable = enable;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public QuestionCategory getQuestionCategory() {
        return questionCategory;
    }

    public void setQuestionCategory(QuestionCategory questionCategory) {
        this.questionCategory = questionCategory;
    }

    public QuestionType getQuestionType() {
        return questionType;
    }

    public void setQuestionType(QuestionType questionType) {
        this.questionType = questionType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public Timestamp getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Timestamp createdate) {
        this.createdate = createdate;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}