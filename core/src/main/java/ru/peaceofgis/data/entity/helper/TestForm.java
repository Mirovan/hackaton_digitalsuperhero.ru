package ru.peaceofgis.data.entity.helper;

import java.util.List;

public class TestForm {
    private List<Solution> solutions;

    public TestForm() {
    }

    public List<Solution> getSolutions() {
        return solutions;
    }

    public void setSolutions(List<Solution> solutions) {
        this.solutions = solutions;
    }

}
