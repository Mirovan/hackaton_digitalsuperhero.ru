package ru.peaceofgis.data.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.*;

@Entity
@Table(name = "answer")
@Transactional
@ApiModel(description = "Ответ а вопрос")
public class Answer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(notes = "Уникальный идентификатор", hidden = true)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_question")
    @ApiModelProperty(notes = "Вопрос")
    private Question question;

    @ApiModelProperty(notes = "Текст ответа")
    @Column(name = "answer_text")
    private String text;

    @ApiModelProperty(notes = "Правильный ли ответ")
    @Column(name = "correct")
    private Boolean correct;

    public Answer() {
    }

    public Answer(Question question, String text, Boolean correct) {
        this.question = question;
        this.text = text;
        this.correct = correct;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getCorrect() {
        return correct;
    }

    public void setCorrect(Boolean correct) {
        this.correct = correct;
    }
}