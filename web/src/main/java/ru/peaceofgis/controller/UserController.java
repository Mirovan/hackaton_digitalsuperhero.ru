package ru.peaceofgis.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.peaceofgis.data.entity.AppointUserTest;
import ru.peaceofgis.data.entity.User;
import ru.peaceofgis.service.iface.AppointUserTestService;
import ru.peaceofgis.service.iface.UserAttemptService;
import ru.peaceofgis.service.iface.UserService;

import java.security.Principal;
import java.util.List;

@Controller
public class UserController {

	private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private UserService userService;

	@Autowired
	private AppointUserTestService appointUserTestService;

	@Autowired
	private UserAttemptService userAttemptService;

	/**
	 * Главная страница профиля пользователя
	 * */
	@RequestMapping(value="/user/", method = RequestMethod.GET)
	public ModelAndView showUserPage(Principal principal) {
		User authUser = userService.getByEmail( principal.getName() );
		if (authUser != null) {
			ModelAndView modelAndView = new ModelAndView();
			modelAndView.setViewName("user/index");
			return modelAndView;
		} else {
			return new ModelAndView("redirect:/login/");
		}
	}


	/**
	 * Список всех назначенных пользователю тестов
	 * */
	@RequestMapping(value="/user/tests/", method = RequestMethod.GET)
	public ModelAndView showTests(Principal principal) {
		User authUser = userService.getByEmail( principal.getName() );
		if (authUser != null) {
			List<AppointUserTest> appointUserTests = appointUserTestService.getByUser(authUser);
			for (AppointUserTest appointUserTest: appointUserTests) {
				appointUserTest.setUserAttempts( userAttemptService.getByAppoint(appointUserTest) );
			}
			ModelAndView modelAndView = new ModelAndView();
			modelAndView.addObject("appointUserTests", appointUserTests);
			modelAndView.setViewName("user/tests/index");
			return modelAndView;
		} else {
			return new ModelAndView("redirect:/error/403/");
		}
	}

}
