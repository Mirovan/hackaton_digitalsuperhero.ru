package ru.peaceofgis.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.peaceofgis.data.entity.helper.AttemptStatus;
import ru.peaceofgis.data.entity.*;
import ru.peaceofgis.service.iface.*;

import javax.validation.Valid;
import java.beans.PropertyEditorSupport;
import java.security.Principal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;


@Controller
public class CabinetController {

    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private TestService testService;

    @Autowired
    private AppointUserTestService appointUserTestService;

    @Autowired
    private UserAttemptService userAttemptService;

    @Autowired
    private AttemptResultService attemptResultService;

    /**
     * Биндинг метод для игнорирования null-полей дат
     * */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Timestamp.class,
                new PropertyEditorSupport() {
                    public void setAsText(String value) {
                        try {
                            Date parsedDate = new SimpleDateFormat("dd.MM.yyyy").parse(value);
                            setValue(new Timestamp(parsedDate.getTime()));
                        } catch (ParseException e) {
                            setValue(null);
                        }
                    }
                });
    }


    /**
     * Главная страница Кабинета администратора
     * */
    @RequestMapping(value="/cabinet/", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView showCabinet(Principal principal) {
        User authUser = userService.getByEmail(principal.getName());
        //Если доступ есть
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("cabinet/index");
        return modelAndView;
    }


    /**
     * Список пользователей
     * */
    @RequestMapping(value="/cabinet/users/", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView showUsers() {
        ModelAndView modelAndView = new ModelAndView();
        List<User> users = userService.getAllByOrder("ASC");

        modelAndView.addObject("users", users);
        modelAndView.setViewName("cabinet/users");
        return modelAndView;
    }


    /**
     * Форма для Добавления пользователя
     * */
    @RequestMapping(value="/cabinet/users/add/", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView addUserForm() {
        ModelAndView modelAndView = new ModelAndView();
        User user = new User();
        user.setPassword( userService.generateRandomPassword() );
        modelAndView.addObject("user", user);
        modelAndView.addObject("action", "add");
        modelAndView.setViewName("cabinet/users/edit");
        return modelAndView;
    }


    /**
     * Добавления пользователя
     * */
    @RequestMapping(value="/cabinet/users/add/", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView addUserPost(@ModelAttribute @Valid User user, BindingResult bindingResult) {
        //Если пользователь с таким email уже есть
        User userExists = userService.getByEmail(user.getEmail());
        if (userExists != null) {
            bindingResult
                    .rejectValue("email", "error.user",
                            "There is already a user registered with the email provided");
        }
        //Есть ошибки - открываем форму повторно
        if (bindingResult.hasErrors()) {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.addObject("user", user);
            modelAndView.addObject("action", "add");
            modelAndView.setViewName("/cabinet/users/edit");
            return modelAndView;
        } else {
            user.setPassword(userService.encodePassword(user.getPassword()));
            user.setActive(true);
            user.setRoles(Collections.singleton(roleService.findRoleByCode("DEFAULT")));
            userService.add(user);

            return new ModelAndView("redirect:/cabinet/users/");
        }
    }


    /**
     * Форма для Редактирования пользователя
     * */
    @RequestMapping(value="/cabinet/users/{id}/edit/", method = RequestMethod.GET)
    public ModelAndView editUserForm(@PathVariable("id") Integer id) {
        ModelAndView modelAndView = new ModelAndView();
        User user = userService.getById(id);
        user.setPassword("");
        modelAndView.addObject("user", user);
        modelAndView.addObject("action", "edit");
        modelAndView.setViewName("/cabinet/users/edit");
        return modelAndView;
    }


    /**
     * Редактирование пользователя
     * */
    @RequestMapping(value="/cabinet/users/{id}/edit/", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView editUserPost(@PathVariable Integer id, @ModelAttribute @Valid User updateUser, BindingResult bindingResult) {
        User user = userService.getById(id);    //Кого обновляем

        //Если пользователь с таким email уже есть
        User userExists = userService.getByEmail(user.getEmail());
        if (userExists != null) {
            bindingResult
                    .rejectValue("email", "error.user",
                            "There is already a user registered with the email provided");
        }
        //Есть ошибки - открываем форму повторно
        if (bindingResult.hasErrors()) {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.addObject("user", user);
            modelAndView.addObject("action", id + "/edit");
            modelAndView.setViewName("/cabinet/users/edit");
            return modelAndView;
        } else {
            userService.update(user, updateUser);
            return new ModelAndView("redirect:/cabinet/users/");
        }
    }


    /**
     * Форма для Добавления теста пользователю
     * */
    @RequestMapping(value="/cabinet/users/{id}/test/add/", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView addTestForm(@PathVariable Integer id, Principal principal) {
        User authUser = userService.getByEmail( principal.getName() );
        User user = userService.getById(id);
        List<Test> tests = testService.getAll();

        AppointUserTest appointUserTest = new AppointUserTest();
        appointUserTest.setUser(user);
        appointUserTest.setAppointer(authUser);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("userId", user.getId());
        modelAndView.addObject("appointUserTest", appointUserTest);
        modelAndView.addObject("tests", tests);
        modelAndView.setViewName("cabinet/users/test/edit");
        return modelAndView;
    }


    /**
     * Добавление теста пользователю
     * */
    @RequestMapping(value="/cabinet/users/{id}/test/add/", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView postAddTest(@ModelAttribute AppointUserTest appointUserTest, Principal principal) {
        User authUser = userService.getByEmail( principal.getName() );
        appointUserTest.setTest(testService.getById(appointUserTest.getTest().getId()));
        appointUserTest.setAppointer(authUser);
        appointUserTest.setUser(userService.getById(appointUserTest.getUser().getId()));
        appointUserTest.setAppointDate( new Timestamp(Calendar.getInstance().getTime().getTime()) );

        appointUserTestService.add(appointUserTest);
        return new ModelAndView("redirect:/cabinet/users/" + appointUserTest.getUser().getId() + "/tests/");
    }


    /**
     * Просмотр назначенных тестов пользователю
     * */
    @RequestMapping(value="/cabinet/users/{id}/tests/", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView showUserTests(@PathVariable Integer id) {
        List<AppointUserTest> appointUserTests = appointUserTestService.getAll();
        //Получаем все попытки прохождения теста для этого назначения
        for (AppointUserTest appointUserTest: appointUserTests) {
            List<UserAttempt> userAttempts = userAttemptService.getByAppoint(appointUserTest);
            for (UserAttempt attempt: userAttempts) {
                AttemptResult attemptResult = attemptResultService.getByAttempt(attempt);
                attempt.setAttemptResult(attemptResult);

                //Определяем попытка закончена или нет
                Timestamp finishTestTime = null;
                if (attempt.getAppointUserTest().getTest().getDuration() != null) {
                    //Формируем время окончания теста, если есть ограничение о времени
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(attempt.getAppointUserTest().getTest().getDuration());
                    int hour = calendar.get(Calendar.HOUR_OF_DAY);
                    int minute = calendar.get(Calendar.MINUTE);
                    int second = calendar.get(Calendar.SECOND);

                    finishTestTime = new Timestamp(attempt.getStartdate().getTime());
                    finishTestTime.setTime(finishTestTime.getTime() + (1000 * 60 * 60 * hour));  //add hour
                    finishTestTime.setTime(finishTestTime.getTime() + (1000 * 60 * minute));  //add minute
                    finishTestTime.setTime(finishTestTime.getTime() + (1000 * second));  //add second

                    Timestamp now = new Timestamp(Calendar.getInstance().getTime().getTime());
                    //Срок жизни попытки теста вышел - попытка завершена
                    if (now.compareTo(finishTestTime) > 0) {
                        attempt.setAttemptStatus(AttemptStatus.FINISHED);
                    } else {
                        attempt.setAttemptStatus(AttemptStatus.IN_PROGRESS);
                    }
                }

            }
            appointUserTest.setUserAttempts(userAttempts);
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("appointUserTests", appointUserTests);
        modelAndView.setViewName("cabinet/users/tests");
        return modelAndView;
    }

}