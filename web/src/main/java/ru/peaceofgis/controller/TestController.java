package ru.peaceofgis.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.peaceofgis.data.entity.*;
import ru.peaceofgis.data.entity.helper.Solution;
import ru.peaceofgis.data.entity.helper.TestForm;
import ru.peaceofgis.service.iface.*;

import java.security.Principal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;


@Controller
public class TestController {

    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private TestService testService;

    @Autowired
    private AnswerService answerService;

    @Autowired
    private UserAttemptService userAttemptService;

    @Autowired
    private AttemptQuestionService attemptQuestionService;

    @Autowired
    private AttemptSolutionService attemptSolutionService;

    @Autowired
    private UserService userService;

    @Autowired
    private AppointUserTestService appointUserTestService;

    @Autowired
    private AttemptResultService attemptResultService;


    /**
     * Отображаем информацию о подготовке в запуске тестирования
     * */
    @RequestMapping(value="/test/appoint/{id}/", method = RequestMethod.GET)
    public ModelAndView showPrepareRunTest(@PathVariable Integer id, Principal principal) {
        User authUser = userService.getByEmail( principal.getName() );
        if (authUser != null) {
            //Если пользователь имеет доступ к назначенному тесту
            AppointUserTest appointUserTest = appointUserTestService.getById(id);
            if ( appointUserTest.getUser().getId().equals(authUser.getId()) ) {
                ModelAndView modelAndView = new ModelAndView();
                appointUserTest.setUserAttempts( userAttemptService.getByAppoint(appointUserTest) );
                modelAndView.addObject(appointUserTest);
                modelAndView.setViewName("test/appoint/index");
                return modelAndView;
            } else {
                return new ModelAndView("redirect:/error/403/");
            }
        } else {
            return new ModelAndView("redirect:/login/");
        }
    }


    /**
     * Запуск теста по назначенному id
     * */
    @RequestMapping(value="/test/appoint/{id}/start/", method = RequestMethod.GET)
    public ModelAndView runTest(@PathVariable Integer id, Principal principal) {
        User authUser = userService.getByEmail( principal.getName() );
        if (authUser != null) {
            //Если пользователь имеет доступ к назначенному тесту
            AppointUserTest appointUserTest = appointUserTestService.getById(id);
            if ( appointUserTest.getUser().getId().equals(authUser.getId()) ) {
                //Проверяем - не исчерпано ли число попыток и время прохождения теста
                Timestamp now = new Timestamp(Calendar.getInstance().getTime().getTime());
                boolean canAttempt = true;
                //Если текущая дата меньше даты начала теста
                if ( appointUserTest.getStartDate() != null && now.compareTo(appointUserTest.getStartDate()) < 0) {
                    canAttempt = false;
                }
                //Если текущая дата больше даты окончания теста
                if ( appointUserTest.getEndDate() != null && now.compareTo(appointUserTest.getEndDate()) > 0) {
                    canAttempt = false;
                }
                //Если число попыток превышено
                List<UserAttempt> userAttempts = userAttemptService.getByAppoint(appointUserTest);
                if ( appointUserTest.getMaxAttemptCount() != null &&
                        userAttempts.size() >= appointUserTest.getMaxAttemptCount() ) {
                    canAttempt = false;
                }

                if (canAttempt) {

                    //создаем тест
                    Test test = testService.getById(appointUserTest.getTest().getId());

                    //создаем попытку пользователя
                    UserAttempt userAttempt = new UserAttempt(null, appointUserTest, new Timestamp(Calendar.getInstance().getTime().getTime()), null);
                    userAttemptService.add(userAttempt);

                    //создаём список вопросов для текущего теста
                    List<AttemptQuestion> attemptQuestions = attemptQuestionService.getRandomByTest(test, userAttempt, test.getMaxQuestions());
                    attemptQuestionService.add(attemptQuestions);

                    return new ModelAndView("redirect:/test/attempt/" + userAttempt.getUid() + "/solve/");
                } else {
                    System.out.println("Число попыток превышено или тест не доступен по дате");
                    return new ModelAndView("redirect:/user/");
                }
            } else {
                return new ModelAndView("redirect:/error/403/");
            }
        } else {
            return new ModelAndView("redirect:/login/");
        }
    }


    /**
     * Отображение теста для попытки пользователя
     * */
    @RequestMapping(value="/test/attempt/{uid}/solve/", method = RequestMethod.GET)
    public ModelAndView showUserAttemptTestForm(@PathVariable("uid") String userAttemptUID, Principal principal) {
        User authUser = userService.getByEmail( principal.getName() );
        if (authUser != null) {

            UserAttempt userAttempt = userAttemptService.getByUID(userAttemptUID);
            //Если пользователь имеет доступ к этой попытке
            if ( userAttempt.getAppointUserTest().getUser().getId().equals(authUser.getId()) ) {
                Timestamp now = new Timestamp(Calendar.getInstance().getTime().getTime());

                //Если тест уже завершен
                if (userAttempt.getEnddate() != null && now.compareTo(userAttempt.getEnddate()) > 0) {
                    return new ModelAndView("redirect:/error/test/");
                }

                //Проверяем не вышло ли время отведенное на тест
                Timestamp finishTestTime = null;
                if (userAttempt.getAppointUserTest().getTest().getDuration() != null) {
                    //Формируем время окончания теста, если есть ограничение о времени
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(userAttempt.getAppointUserTest().getTest().getDuration());
                    int hour = calendar.get(Calendar.HOUR_OF_DAY);
                    int minute = calendar.get(Calendar.MINUTE);
                    int second = calendar.get(Calendar.SECOND);

                    finishTestTime = new Timestamp(userAttempt.getStartdate().getTime());
                    finishTestTime.setTime(finishTestTime.getTime() + (1000 * 60 * 60 * hour));  //add hour
                    finishTestTime.setTime(finishTestTime.getTime() + (1000 * 60 * minute));  //add minute
                    finishTestTime.setTime(finishTestTime.getTime() + (1000 * second));  //add second

                    if (now.compareTo(finishTestTime) > 0) {
                        return new ModelAndView("redirect:/error/test/");
                    }
                }

                //Выше все ОК - отображем тест
                Test test = userAttempt.getAppointUserTest().getTest();

                //Создаем обёртку - форму теста
                TestForm testForm = new TestForm();

                List<AttemptQuestion> attemptQuestions = attemptQuestionService.getByAttempt(userAttempt);

                //получаем все ответы на вопросы теста
                for (AttemptQuestion aq : attemptQuestions) {
                    aq.getQuestion().setAnswers(answerService.getByQuestion(aq.getQuestion()));
                }

                ModelAndView modelAndView = new ModelAndView();
                modelAndView.addObject("test", test);
                modelAndView.addObject("userAttempt", userAttempt);
                modelAndView.addObject("attemptQuestions", attemptQuestions);
                modelAndView.addObject("testForm", testForm);

                //Формируем время окончания теста, если есть ограничение о времени
                if (test.getDuration() != null) {
                    modelAndView.addObject("finishTestTime", finishTestTime);
                    modelAndView.addObject("nowTime", now);
                } else {
                    modelAndView.addObject("finishTestTime", "");
                    modelAndView.addObject("nowTime", "");
                }

                modelAndView.setViewName("test/attempt/index");
                return modelAndView;
            } else {
                return new ModelAndView("redirect:/error/403/");
            }
        } else {
            return new ModelAndView("redirect:/login/");
        }
    }


    /**
     * Обрабатываем отправку ответов пользователя
     * */
    @RequestMapping(value="/test/attempt/{uid}/solve/", method = RequestMethod.POST)
    public ModelAndView postTestAttempt(@PathVariable("uid") String userAttemptUID,
                                        @ModelAttribute TestForm testForm) {
        //Получаем попытку пользователя
        UserAttempt userAttempt = userAttemptService.getByUID(userAttemptUID);

        //Проверяем не завершена ли еще эта попытка
        Timestamp now = new Timestamp(Calendar.getInstance().getTime().getTime());
        if ( userAttempt.getEnddate() != null && now.compareTo(userAttempt.getEnddate()) > 0 ) {
            return new ModelAndView("redirect:/error/test/");
        }

        //Проверяем не вышло ли время отведенное на тест
        if (userAttempt.getAppointUserTest().getTest().getDuration() != null) {
            //Формируем время окончания теста, если есть ограничение о времени
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(userAttempt.getAppointUserTest().getTest().getDuration());
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);
            int second = calendar.get(Calendar.SECOND);

            Timestamp finishTestTime = new Timestamp(userAttempt.getStartdate().getTime());
            finishTestTime.setTime(finishTestTime.getTime() + (1000 * 60 * 60 * hour));  //add hour
            finishTestTime.setTime(finishTestTime.getTime() + (1000 * 60 * minute));  //add minute
            finishTestTime.setTime(finishTestTime.getTime() + (1000 * second));  //add second

            if (now.compareTo(finishTestTime) > 0) {
                return new ModelAndView("redirect:/error/test/");
            }
        }

        //Все ОК - попытка еще действует

        //указываем что тестирование закончено
        userAttempt.setEnddate(new Timestamp(Calendar.getInstance().getTime().getTime()));
        userAttemptService.update(userAttempt);

        //сохраняем ответы пользователя
        List<Solution> solutions = testForm.getSolutions();
        if (solutions != null) {
            for (Solution solution : solutions) {
                if (solution != null) {
                    List<Integer> answerIDs = solution.getAnswerIds();
                    if (answerIDs != null) {
                        for (Integer answerId : answerIDs) {
                            AttemptSolution attemptSolution = new AttemptSolution(
                                    attemptQuestionService.getById(solution.getAttemptQuestionId()),
                                    answerService.getById(answerId)
                            );
                            attemptSolutionService.add(attemptSolution);
                        }
                    }
                }
            }
        }

        return new ModelAndView("redirect:/test/attempt/" + userAttempt.getUid() + "/result/");
    }


    @RequestMapping(value="/test/attempt/{uid}/result/", method = RequestMethod.GET)
    public ModelAndView postTestAttempt(@PathVariable("uid") String userAttemptUID) {
        UserAttempt userAttempt = userAttemptService.getByUID(userAttemptUID);

        //Получаем результат попытки пользователя
        AttemptResult attemptResult = userAttemptService.getResult(userAttempt);
        attemptResultService.add(attemptResult);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("userAttempt", userAttempt);
        modelAndView.addObject("attemptResult", attemptResult);
        modelAndView.setViewName("test/attempt/result");
        return modelAndView;
    }

}